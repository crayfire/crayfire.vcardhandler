﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Crayfire.Components.vCard
{
    public enum TelephoneNumberType
    {
        PREF,
        WORK,
        HOME,
        VOICE,
        FAX,
        MSG,
        CELL,
        PAGER,
        BBS,
        MODEM,
        CAR
    }

    public enum AddressType
    {
        DOM,
        INTL,
        POSTAL,
        PARCEL,
        HOME,
        WORK
    }

    public enum EmailType
    {
        HOME,
        WORK
    }

    /// <summary>
    /// This class contains all methods to create, use, e.g. a vCard
    /// </summary>
    public class vCardHandler
    {
        vCardStorage cardStorage = new vCardStorage();

        public void AddPhoneNumber(string Number, TelephoneNumberType PhoneType)
        {
            cardStorage.telephoneNumbers.Add(new TelephoneNumber { PhoneNumber = Number, PhoneNumberType = PhoneType });
        }

        public void AddAddress(string street, string zipCode, string city, string country, AddressType deliveryAddressType)
        {
            cardStorage.deliveryAddresses.Add(new DeliveryAddress {Street = street, ZipCode = zipCode, Locality = city, Country = country, DeliveryAddressType = deliveryAddressType });
        }

        public void AddMailAddress(string mailAddress, EmailType emailType)
        {
            cardStorage.emailAddresses.Add(new EmailAddresses { MailAddress = mailAddress, MailAddressType = emailType });
        }

        public float vCardVersion
        {
            get { return cardStorage.vCardVersion; }
            set { cardStorage.vCardVersion = value; }
        }

        public string FormattedName
        { 
            get { return cardStorage.FormattedName; }
            set { cardStorage.FormattedName = value; }
        }

        public string FirstName
        {
            get { return cardStorage.FirstName;  }
            set { cardStorage.FirstName = value; }
        }

        public string MiddleName
        {
            get { return cardStorage.MiddleName; }
            set { cardStorage.MiddleName = value; }
        }

        public string LastName
        {
            get { return cardStorage.LastName; }
            set { cardStorage.LastName = value; }
        }

        public string NamePrefix
        {
            get { return cardStorage.NamePrefix ; }
            set { cardStorage.NamePrefix = value; }
        }

        public string NameSuffix
        {
            get { return cardStorage.NameSuffix; }
            set { cardStorage.NameSuffix = value; }
        }

        public string Organization
        {
            get { return cardStorage.Organization; }
            set { cardStorage.Organization = value; }
        }

        public string Role
        {
            get { return cardStorage.Role; }
            set { cardStorage.Role = value; }
        }

        public string Title
        {
            get { return cardStorage.Title; }
            set { cardStorage.Title = value; }
        }

        public string Note
        {
            get { return cardStorage.Note; }
            set { cardStorage.Note = value; }
        }

        public DateTime BirthDate
        {
            get { return cardStorage.BirthDate; }
            set { cardStorage.BirthDate = value; }
        }

        /// <summary>
        /// Speichert eine vCard physisch auf dem Datenträger.
        /// </summary>
        /// <remarks>Bitte Escapen</remarks>
        /// <param name="Version">Gibt die Version der vCard an. Gültige Werte sind: 2.1, 3.0., 4.0</param>
        /// <param name="FilePath">Den Speicherort, wo die vCard gespeichert werden soll. Bsp.: C:\\Temp\\MeinevCard.vcf</param>
        /// <param name="card">Eine <see cref="vCard"/> die erzeugt werden soll.</param>
        //public static void CreateNewvCard(float Version, string FilePath, vCardStorage card)
        //{
        //    vCardStorage vCard = new vCardStorage();
        //    FileStream fileStream = new FileStream(FilePath, FileMode.CreateNew, FileAccess.Write);
        //    fileStream.Close();

        //    vCard.vCardVersion = Version;
        //    vCard.FirstName = card.FirstName;
        //    vCard.LastName = card.LastName;
        //    vCard.NamePrefix = card.NamePrefix;
        //    if (card.FormattedName != null)
        //    {
        //        vCard.FormattedName = card.FormattedName;
        //    }
        //    vCard.Organization = card.Organization;
        //    vCard.Role = card.Role;
        //    vCard.BirthDate = card.BirthDate;
        //    vCard.PhoneNumbers = card.PhoneNumbers;
        //    vCard.Addresses = card.Addresses;
        //    vCard.EmailAddresses = card.EmailAddresses;

        //    try
        //    {
        //        SavevCard(FilePath, vCard);
        //    }
        //    catch (Exception Ex)
        //    {
        //        Console.WriteLine(Ex.ToString());
        //        throw;
        //    }
        //}

        /// <summary>
        /// Erstellt eine vCard nach dem Vorbild der Wikipedia Standard vCard.
        /// </summary>
        /// <remarks>Diese Methode ist nur für Debug Zwecke gedacht. Es wird eine vCard Version 4.0 in ASCII erzeugt</remarks>
        //public static void CreateNewvCardRandom(string FilePath)
        //{
        //    float Version = 4.0F;
        //    FileStream fileStream = new FileStream(FilePath, FileMode.CreateNew, FileAccess.Write);
        //    fileStream.Close();

        //    vCardStorage Card = new vCardStorage
        //    {
        //        vCardVersion = Version,
        //        FirstName = "Erika",
        //        LastName = "Mustermann",
        //        NamePrefix = "Dr.",
        //        //Card.FormattedName = Card.Title + " " + Card.FirstName + " " + Card.LastName;
        //        Organization = "Crayfire GbR",
        //        Role = "Geschäftsführer",
        //        PhoneNumbers = new List<TelephoneNumber>
        //    {
        //        new vCardStorage.TelephoneNumber() { PhoneNumber = "+49-221-9999123" , PhoneNumberType = TelephoneNumberType.HOME }
        //    },
        //        Addresses = new List<DeliveryAddress>
        //    {
        //        new vCardStorage.DeliveryAddress() { DeliveryAddressType =  HOME, Country = "Germany", ZipCode = "51147", Locality = "Köln", Street = "Heidestraße 17" , Region = "Nordrhein-Westfalen"}
        //    },
        //        EmailAddresses = new List<string>
        //    {
        //        "erika@crayfire.de"
        //    }
        //    };

        //    SavevCard(FilePath, Card);

        //}

        //public static void SavevCard(string FilePath, vCardStorage card)
        //{

        //    if (card.vCardVersion == 2.1f)
        //    {
        //        StreamWriter streamWriter = new StreamWriter(FilePath, true, Encoding.ASCII);
        //        using (streamWriter)
        //        {
        //            try
        //            {
        //                var utf8bytes = Testkatze(card);
        //                streamWriter.Write(Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,Encoding.ASCII, Encoding.UTF8.GetBytes(utf8bytes))));
        //            }
        //            catch (Exception Ex)
        //            {
        //                Console.WriteLine(Ex.ToString());
        //                throw;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        StreamWriter streamWriter2 = new StreamWriter(FilePath,true, new UTF8Encoding(false));
        //        using (streamWriter2)
        //        {
        //            try
        //            {
        //                streamWriter2.Write(Testkatze(card));
        //            }
        //            catch (Exception Ex)
        //            {
        //                Console.WriteLine(Ex.ToString());
        //                throw;
        //            }
        //        }
        //    }



        //}

        
        
        public string vCardContent()
        {
            StringBuilder stringBuilder = new StringBuilder();
            try
            {
                stringBuilder.Append("BEGIN:VCARD" + Environment.NewLine);
                stringBuilder.Append("VERSION:" + cardStorage.vCardVersion.ToString() + Environment.NewLine);
                if (cardStorage.FormattedName == null)
                {
                    stringBuilder.Append("FN:" + cardStorage.NamePrefix + " " + cardStorage.FirstName + " " + cardStorage.LastName + Environment.NewLine);
                }
                else
                {
                    stringBuilder.Append("FN:" + cardStorage.FormattedName + Environment.NewLine);
                }
                stringBuilder.Append("N:" + cardStorage.LastName + ";");
                stringBuilder.Append(cardStorage.FirstName + ";");
                stringBuilder.Append(cardStorage.MiddleName + ";");
                stringBuilder.Append(cardStorage.NamePrefix + ";");
                stringBuilder.Append(cardStorage.NameSuffix + Environment.NewLine);

                if (cardStorage.BirthDate != DateTime.MinValue)
                {
                    stringBuilder.Append("BDAY:" + cardStorage.BirthDate.ToString("yyyyMMdd") + Environment.NewLine);
                }

                foreach (DeliveryAddress da in cardStorage.deliveryAddresses)
                {
                    
                    stringBuilder.Append("ADR;TYPE="+ da.DeliveryAddressType.ToString() + ":" + da.PostOfficeBox + ";" + da.ExtendedAddress + ";" + da.Street + ";" + da.Locality + ";" + da.Region + ";" + da.ZipCode + ";" + da.Country + Environment.NewLine );
                }

                foreach (TelephoneNumber phone in cardStorage.telephoneNumbers)
                {
                    stringBuilder.Append("TEL;TYPE="+ phone.PhoneNumberType.ToString() +",VOICE:" + phone.PhoneNumber.ToString() + Environment.NewLine);
                }

                foreach (EmailAddresses email in cardStorage.emailAddresses)
                {
                    stringBuilder.Append("EMAIL;TYPE=" + email.MailAddressType.ToString() + ":" + email.MailAddress + Environment.NewLine);
                }
                stringBuilder.Append("TITLE:" + cardStorage.Title + Environment.NewLine);
                stringBuilder.Append("ROLE:" + cardStorage.Role + Environment.NewLine);
                stringBuilder.Append("ORG:" + cardStorage.Organization + Environment.NewLine);
                stringBuilder.Append("END:VCARD" + Environment.NewLine);

                return stringBuilder.ToString();
            }
            catch (Exception)
            {
               
                throw;
            }
        }

        //public static string Testkatze(vCardStorage card)
        //{
        //    try
        //    {
        //        StringBuilder stringBuilder = new StringBuilder();
        //        stringBuilder.Append("BEGIN:VCARD" + Environment.NewLine);
        //        stringBuilder.Append("VERSION:" + card.vCardVersion.ToString() + Environment.NewLine);

        //        if (card.FormattedName == null)
        //        {
        //            stringBuilder.Append("FN:" + card.NamePrefix + " " + card.FirstName + " " + card.LastName + Environment.NewLine);
        //        }
        //        else
        //        {
        //            stringBuilder.Append("FN:" + card.FormattedName + Environment.NewLine);
        //        }

        //        stringBuilder.Append("N:" + card.LastName + ";");
        //        stringBuilder.Append(card.FirstName + ";");
        //        stringBuilder.Append(card.MiddleName + ";");
        //        stringBuilder.Append(card.NamePrefix + ";");
        //        stringBuilder.Append(card.NameSuffix + Environment.NewLine);

        //        if (card.BirthDate != DateTime.MinValue)
        //        {
        //            stringBuilder.Append("BDAY:" + card.BirthDate.ToString("yyyyMMdd") + Environment.NewLine);
        //        }

        //        foreach (vCardStorage.DeliveryAddress da in card.Addresses)
        //        {
        //            stringBuilder.Append(da.ToString());
        //        }

        //        foreach (vCardStorage.TelephoneNumber phone in card.PhoneNumbers)
        //        {
        //            stringBuilder.Append(phone.ToString());
        //        }

        //        foreach (string email in card.EmailAddresses)
        //        {
        //            stringBuilder.Append("EMAIL:" + email + Environment.NewLine);
        //        }

        //        stringBuilder.Append("TITLE:" + card.Title + Environment.NewLine);
        //        stringBuilder.Append("ROLE:" + card.Role + Environment.NewLine);
        //        stringBuilder.Append("ORG:" + card.Organization + Environment.NewLine);
        //        stringBuilder.Append("END:VCARD" + Environment.NewLine);

        //        return stringBuilder.ToString();
        //    }
        //    catch (Exception Ex)
        //    {
        //        Console.WriteLine(Ex.ToString());
        //        throw;
        //    }
        //}

        /// <summary>
        /// Liest eine vCard Datei von einem angeben Pfad.
        /// </summary>
        /// <remarks>Bitte Escapen</remarks>
        /// <param name="FilePath">Den Speicherort, von wo die vCard gelesen werden soll. Bsp.: C:\\Temp\\MeinevCard.vcf</param>
        /// <returns>Die vCard als eigenes <see cref="vCard"/> Objekt</returns>
        //        public static vCard ReadvCardFromFile(string FilePath)
        //        {
        //            vCard card = new vCard();
        //            string[] vcardfile = null;
        //            if (!File.Exists(FilePath))
        //            {
        //                Console.WriteLine("ERROR: Datei wurde nicht gefunden!");
        //            }
        //            else
        //            {
        //                try
        //                {
        //                    vcardfile = System.IO.File.ReadAllLines(FilePath);
        //                    if (vcardfile.Length == 0)
        //                    {
        //                        Console.WriteLine("ERROR: vCard ist leer.");
        //                    }
        //                }
        //                catch (Exception)
        //                {

        //                    throw;
        //                }

        //                card.Addresses = new List<DeliveryAddress>() { };
        //                card.PhoneNumbers = new List<TelephoneNumber>() { };

        //                foreach (string line in vcardfile)
        //                {
        //                    Console.WriteLine("\t" + line);

        //                    if (line.StartsWith("VERSION:"))
        //                    {
        //                        card.vCardVersion = float.Parse(line.TrimStart('V', 'E', 'R', 'S', 'I', 'O', 'N', ':'), CultureInfo.InvariantCulture.NumberFormat);
        //#if DEBUG
        //                        Console.WriteLine("DEBUG: " + card.vCardVersion.ToString());
        //#endif
        //                    }

        //                    if (line.StartsWith("N:"))
        //                    {
        //                        string[] split = line.Split(new char[] { ';' });
        //                        split[0] = split[0].TrimStart('N', ':');
        //                        card.LastName = split[0];
        //                        card.FirstName = split[1];
        //                        //card.SecondFirstName = split[2];
        //                        card.Title = split[3];
        //                        //card.SecondFirstName = split[4];
        //#if DEBUG
        //                        foreach (String splitted in split)
        //                        {
        //                            Console.WriteLine("\tDEBUG: " + splitted);

        //                        }
        //#endif
        //                    }

        //                    if (line.StartsWith("FN:"))
        //                    {
        //                        card.FormattedName = line.TrimStart('F', 'N', ':');
        //                    }

        //                    if (line.StartsWith("EMAIL:"))
        //                    {
        //                        card.EmailAddresses = new List<string> { line.TrimStart('E', 'M', 'A', 'I', 'L', ':') };

        //                        //card.EmailAddresses[].Insert(line.TrimStart('E', 'M', 'A', 'I', 'L', ':'));
        //                    }

        //                    if (line.StartsWith("ORG:"))
        //                    {
        //                        card.Organization = line.TrimStart('O', 'R', 'G', ':');
        //                        card.Organization = card.Organization.TrimEnd(';');

        //                    }

        //                    if (line.StartsWith("TITLE:"))
        //                    {
        //                        card.Title = line.TrimStart('T', 'I', 'T', 'L', 'E', ':');
        //                    }

        //                    if (line.StartsWith("ROLE:"))
        //                    {
        //                        card.Title = line.TrimStart('R', 'O', 'L', 'E', ':');
        //                    }

        //                    if (line.StartsWith("NOTE:"))
        //                    {
        //                        card.Note = line.TrimStart('N', 'O', 'T', 'E', ':');
        //                    }

        //                    if (line.StartsWith("ADR;HOME:"))
        //                    {
        //                        string trimmedstring = line.TrimStart('A', 'D', 'R', ';', 'H', 'O', 'M', 'E');

        //                        string[] split = trimmedstring.Split(new char[] { ';' });

        //                        card.Addresses.Add(new DeliveryAddress()
        //                        {
        //                            DeliveryAddressType = AddressType.HOME,

        //                            PostOfficeBox = split[0].TrimStart(':'),
        //                            ExtendedAddress = split[1],
        //                            Street = split[2],
        //                            Locality = split[3],
        //                            Region = split[4],
        //                            ZipCode = split[5],
        //                            Country = split[6],
        //                        });
        //                    }

        //                    if (line.StartsWith("ADR;WORK:"))
        //                    {
        //                        string trimmedstring = line.TrimStart('A', 'D', 'R', ';', 'W', 'O', 'R', 'K');

        //                        string[] split = trimmedstring.Split(new char[] { ';' });

        //                        card.Addresses.Add(new DeliveryAddress()
        //                        {
        //                            DeliveryAddressType = AddressType.WORK,

        //                            PostOfficeBox = split[0].TrimStart(':'),
        //                            ExtendedAddress = split[1],
        //                            Street = split[2],
        //                            Locality = split[3],
        //                            Region = split[4],
        //                            ZipCode = split[5],
        //                            Country = split[6],
        //                        });
        //                    }

        //                    if (line.StartsWith("TEL;HOME"))
        //                    {
        //                        string trimmedstring = line.TrimStart('T', 'E', 'L', ';', 'H', 'O', 'M', 'E');

        //                        string[] split = trimmedstring.Split(new char[] { ';' });

        //                        card.PhoneNumbers.Add(new TelephoneNumber()
        //                        {
        //                            PhoneNumberType = TelephoneNumberType.HOME,
        //                            PhoneNumber = split[0].TrimStart(':')
        //                        });
        //                    };

        //                    if (line.StartsWith("TEL;WORK"))
        //                    {
        //                        string trimmedstring = line.TrimStart('T', 'E', 'L', ';', 'W', 'O', 'R', 'K');

        //                        string[] split = trimmedstring.Split(new char[] { ';' });

        //                        card.PhoneNumbers.Add(new TelephoneNumber()
        //                        {
        //                            PhoneNumberType = TelephoneNumberType.WORK,
        //                            PhoneNumber = split[0].TrimStart(':')
        //                        });
        //                    };

        //                    if (line.StartsWith("TEL;FAX"))
        //                    {
        //                        string trimmedstring = line.TrimStart('T', 'E', 'L', ';', 'F', 'A', 'X');

        //                        string[] split = trimmedstring.Split(new char[] { ';' });

        //                        card.PhoneNumbers.Add(new TelephoneNumber()
        //                        {
        //                            PhoneNumberType = TelephoneNumberType.FAX,
        //                            PhoneNumber = split[0].TrimStart(':')
        //                        });
        //                    };

        //                    if (line.StartsWith("TEL;CELL") || line.StartsWith("TEL;TYPE=CELL"))
        //                    {
        //                        string trimmedstring = line.TrimStart('T', 'E', 'L', ';', 'C', 'E', 'L', 'L');

        //                        string[] split = trimmedstring.Split(new char[] { ';' });

        //                        card.PhoneNumbers.Add(new TelephoneNumber()
        //                        {
        //                            PhoneNumberType = TelephoneNumberType.CELL,
        //                            PhoneNumber = split[0].TrimStart(':')
        //                        });
        //                    };

        //                }
        //            }
        //            Console.WriteLine("Finished");
        //            return card;
        //        }

    }

    /// <summary>
    /// This class contains all types and data of the vCard
    /// </summary>
    class vCardStorage
    {
        /// <summary>
        /// Ein vCard Objekt
        /// </summary>
        #region
            private float _vCardVersion;
            public float vCardVersion
            {
                get { return _vCardVersion; }
                set { _vCardVersion = value; }
            }

            private string _firstName;
            public string FirstName
            {
                get { return _firstName; }
                set { _firstName = value; }
            }

            private string _formattedName;
            public string FormattedName
            {
                get { return _formattedName; }
                set { _formattedName = value; }
            }

            private string _lastName;
            public string LastName
            {
                get { return _lastName; }
                set { _lastName = value; }
            }

            private string _middleName;
            public string MiddleName
            {
                get { return _middleName; }
                set { _middleName = value; }
            }

            private string _namePrefix;
            public string NamePrefix
            {
                get { return _namePrefix; }
                set { _namePrefix = value; }
            }

            private string _nameSuffix;
            public string NameSuffix
            {
                get { return _nameSuffix; }
                set { _nameSuffix = value; }
            }

            private string _organization;
            public string Organization
            {
                get { return _organization; }
                set { _organization = value; }
            }

            private string _role;
            public string Role
            {
                get { return _role; }
                set { _role = value; }
            }

            private string _title;
            public string Title
            {
                get { return _title; }
                set { _title = value; }
            }

            private string _note;
            public string Note
            {
                get { return _note; }
                set { _note = value; }
            }

            private DateTime _birthDate;

            /// <summary>
            /// Gets or sets the birth date.
            /// </summary>
            /// <value>The birth date.</value>
            public DateTime BirthDate
            {
                get { return _birthDate; }
                set { _birthDate = value; }
            }
        #endregion


        public List<TelephoneNumber> telephoneNumbers = new List<TelephoneNumber>();

        public List<DeliveryAddress> deliveryAddresses = new List<DeliveryAddress>();

        public List<EmailAddresses> emailAddresses = new List<EmailAddresses>();
    }

    /// <summary>
    /// Helper Class for handling phone number types
    /// </summary>
    public class TelephoneNumber
    {

        private string _phoneNumber;
        public TelephoneNumberType PhoneNumberType;


        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        /// <value>The phone number.</value>
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }

        /// <summary>
        /// Gibt einen <see cref="T:System.String"></see> zurück, der den aktuellen <see cref="T:System.Object"></see> darstellt.
        /// </summary>
        /// <returns>
        /// Ein <see cref="T:System.String"></see>, der den aktuellen <see cref="T:System.Object"></see> darstellt.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("TEL;" + Enum.GetName(typeof(TelephoneNumberType), PhoneNumberType) + ":");
            sb.Append(_phoneNumber + Environment.NewLine);

            return sb.ToString();
        }

    }

    /// <summary>
    /// Helper Class for handling address types
    /// </summary>
    public class DeliveryAddress
    {
        private string _country;
        private string _extendedAddress;
        private string _locality;
        private string _zipCode;
        private string _postOfficeBox;
        private string _region;
        private string _street;
        public AddressType DeliveryAddressType;

        /// <summary>
        /// Gets or sets the post office box.
        /// </summary>
        /// <value>The post office box.</value>
        public string PostOfficeBox
        {
            get { return _postOfficeBox; }
            set { _postOfficeBox = value; }
        }

        /// <summary>
        /// Gets or sets the extended address.
        /// </summary>
        /// <value>The extended address.</value>
        public string ExtendedAddress
        {
            get { return _extendedAddress; }
            set { _extendedAddress = value; }
        }

        /// <summary>
        /// Gets or sets the street.
        /// </summary>
        /// <value>The street.</value>
        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }

        /// <summary>
        /// Gets or sets the locality.
        /// </summary>
        /// <value>The locality.</value>
        public string Locality
        {
            get { return _locality; }
            set { _locality = value; }
        }

        /// <summary>
        /// Gets or sets the region.
        /// </summary>
        /// <value>The region.</value>
        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>The postal code.</value>
        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        /// <summary>
        /// Gibt einen <see cref="T:System.String"></see> zurück, der den aktuellen <see cref="T:System.Object"></see> darstellt.
        /// </summary>
        /// <returns>
        /// Ein <see cref="T:System.String"></see>, der den aktuellen <see cref="T:System.Object"></see> darstellt.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("ADR;" + Enum.GetName(typeof(AddressType), DeliveryAddressType) + ":");
            sb.Append(_postOfficeBox + ";");
            sb.Append(_extendedAddress + ";");
            sb.Append(_street + ";");
            sb.Append(_locality + ";");
            sb.Append(_region + ";");
            sb.Append(_zipCode + ";");
            sb.Append(_country + Environment.NewLine);

            return sb.ToString();
        }
    }

    /// <summary>
    /// Helper Class for handling email types
    /// </summary>
    public class EmailAddresses
    {
        public EmailType MailAddressType;
        public string MailAddress { get; set; }
    }
}
