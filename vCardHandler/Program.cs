﻿using System;
using Crayfire.Components.vCard;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vCardHandlerTool
{
    /*
    Die Hauptklasse für vCards mit iegenem vCard Objekt
    Enthält alle Methoden für grundlegende vCard Operation (lesen, schreiben, zufällige vCard erzeugen)
    */
    class Program
    {
        static void Main(string[] args)
        {
            int opcao;
            //int codigoAluno = 51;
            do
            {
                Console.WriteLine("[ 1 ] Read vCard");
                Console.WriteLine("[ 2 ] Create vCard");
                Console.WriteLine("[ 3 ] Create vCard Test");
                Console.WriteLine("[ 0 ] Exit");
                Console.WriteLine("-------------------------------------");
                Console.Write("Bitte wählen: ");
                opcao = Int32.Parse(Console.ReadLine());
                switch (opcao)
                {
                    case 1:
                        ReadvCardMain();
                        break;
                    case 2:
                        CreatevCardMain();
                        break;
                    case 3:
                        CreatevCardMain2();
                        break;
                    default:
                        Quit();
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
            while (opcao != 0);
        }

        private static void Quit()
        {
            Console.WriteLine();
            Console.WriteLine("Programm beendet. Bitte Taste drücken...");
        }

        private static void CreatevCardMain()
        {
            Console.WriteLine();
            Console.WriteLine("Erstelle vCard");
            vCardHandler.CreateNewvCardRandom("C:\\temp\\Erika2.vcf");
            
        }

        private static void CreatevCardMain2()
        {
            Console.WriteLine();
            Console.WriteLine("Erstelle vCard manuell Test");
            vCardHandler.TelephoneNumber telephone = new vCardHandler.TelephoneNumber()
            {
                PhoneNumber = "123456789",
                PhoneNumberType = TelephoneNumberType.HOME
            };

            vCardHandler.DeliveryAddress address = new vCardHandler.DeliveryAddress()
            {
                DeliveryAddressType = AddressType.HOME,
                Street = "Teststraße 1",
                ZipCode = "13349",
                Country = "Germany",
                Locality = "Berlin",
                Region = "Berlin"

            };
            vCardHandler.vCard card = new vCardHandler.vCard
            {
                Addresses = new List<vCardHandler.DeliveryAddress>()
                {
                    address
                },
                EmailAddresses = new List<string>()
                {
                    "info@getpoint.de"
                },
                FirstName = "Sebastian",
                LastName = "Heldt",
                FormattedName = "ddddd hhhhh",
                vCardVersion = 4.0f,
                Title = "Herr",
                Organization = "Organization",
                Role = "ddd",
                BirthDate = new DateTime(2017, 1, 2),
                PhoneNumbers = new List<vCardHandler.TelephoneNumber>()
                {
                    telephone
                },

            };
            vCardHandler.CreateNewvCard(2.1f,"C:\\temp\\TestvCard.vcf",card);

        }

        private static void ReadvCardMain()
        {
            Console.WriteLine();
            Console.WriteLine("vCard wird gelesen");
            vCardHandler.ReadvCardFromFile("C:\\temp\\Christian Eichfeld2.vcf");
            var test = vCardHandler.ReadvCardFromFile("");
        }
    }
}
